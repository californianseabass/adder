import setuptools


setuptools.setup(
    name="californianseabass-adder",  # Replace with your own username
    version="0.0.1",
    author="californianseabass",
    author_email="releasetheseabass@gmail.com",
    description="A small example package",
    long_description="a nice long description",
    # long_description_content_type="text/markdown",
    url="https://github.com/californianseabass/adder",
    # packages=["adder"],
    py_modules=["adder"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
)
