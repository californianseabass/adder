def add_one(x: int) -> int:
    if not isinstance(x, int):
        raise TypeError("must be of type int")
    return x + 1


def add_two(x: int) -> int:
    if not isinstance(x, int):
        raise TypeError("must be of type int")
    return x + 1


def add_three(x: int) -> int:
    if not isinstance(x, int):
        raise TypeError("must be of type int")
    return x + 1


add_one(3)
